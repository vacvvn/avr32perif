/*
 * delay.c
 *
 * Created: 22.10.2015 11:03:12
 *  Author: vvn
 */ 
 /*
������ ����� �� ��������� ����� ��������� ������
(��������, ��� ������ ������� ���������� 60���, ����� ������ ����� �� 500 ����, ����� ���������� 30000000(30 ���������) ������)
 */
#include <avr32/io.h>
#include "delay.h"

void delay_cpu_ticks(unsigned long delay_ticks)
{
	unsigned long start_time;
	unsigned long stop_time;
	start_time = __builtin_mfsr(AVR32_COUNT);
	stop_time = start_time + delay_ticks;// �������� �����
	while(1)
	{
		unsigned long cur_time;
		cur_time = __builtin_mfsr(AVR32_COUNT);
		if(stop_time < start_time)
		{
			// ������������
			if(cur_time < start_time && cur_time > stop_time)
				break;
		}
		else
		{
			if(cur_time > stop_time || cur_time < start_time)
				break;
		}
	}
}