/*
 * RCOSC.c
 *
 * Created: 21.10.2015 14:11:21
 *  Author: vvn
 */ 
 /*
 * PMconfig.c
 * ������������� power manager ��� uc3a0512
 * ���������� sync clock ��� CPU, HSB, PBA, PBB ����� ����:
1. RCOSC(115 kHz)
2. OSC0(450 kHz-16MHz crystal)
3. PLL0(80-240 MHz)
   ��� PLL0 ���������� ����� ����:
	1. OSC0
	2. OSC1
 * ���������� generic clock ��� ������� ������ ����� ����:
 *	1.OSC0
	2.OSC1
	3.PLL0
	4.PLL1
	��� PLL0 ���������� ����� ����:
		1.OSC0
		2.OSC1
	��� PLL1 ���������� ����� ����:
		1.OSC0
		2.OSC1
 * Created: 21.10.2015 10:05:12
 * Author : vvn
 */
#include "RCOSC.h"
void Init(void)
{
	///PM init start
	AVR32_PM.MCCTRL.osc0en = 0;// ��������� osc0
	//set prescalers for domains clock
    // src_clock - clock from osc0/1, pll0,1, ROSC
	//PB(A/B)DIV = 0: PB(A/B) clock equals src_clock.
	//PB(A/B)DIV = 1: PB(A/B) clock equals src_clock divided by 2^(***SEL+1)
	// hsbdiv cpudiv - ��� uc3b0512 ���������
	// �� ������ ����� 115200 / 2 =  57600 hz
	AVR32_PM.CKSEL.cpudiv = 1;// �����
	AVR32_PM.CKSEL.cpusel = 0;// src_clock / (2^(cpusel+1)) 
	AVR32_PM.CKSEL.hsbdiv = 1; // �����
	AVR32_PM.CKSEL.hsbsel = 0;// src_clock / (2^(hsbsel+1)) 	
	AVR32_PM.CKSEL.pbadiv = 1;// �����
	AVR32_PM.CKSEL.pbasel = 0;// src_clock / (2^(pbasel+1)) 
	AVR32_PM.CKSEL.pbbdiv = 1;// �����
	AVR32_PM.CKSEL.pbbsel = 0;// src_clock / (2^(pbbsel+1)) 
	// sysclk set source
	//Main Clock Select
	//0: The slow clock is the source for the main clock
	//1: Oscillator 0 is source for the main clock
	//2: PLL0 is source for the main clock
	//3: Reserved
	AVR32_PM.MCCTRL.mcsel = AVR32_PM_MCSEL_SLOW;// rcosc
	//////////////////////PM init end
}
