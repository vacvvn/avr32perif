/*
 * delay.h
 *
 * Created: 22.10.2015 11:03:26
 *  Author: vvn
 */ 


#ifndef DELAY_H_
#define DELAY_H_
#ifndef FCPU_HZ
#define  FCPU_HZ 60000000ULL
#endif


void delay_cpu_ticks(unsigned long delay);


#endif /* DELAY_H_ */