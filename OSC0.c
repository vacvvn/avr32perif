/*
 * OSC0.c
 * OSC0 config
 * Created: 21.10.2015 17:03:33
 *  Author: vvn
 */ 
 /*
 * PMconfig.c
 * ������������� power manager ��� uc3a0512
 * ���������� sync clock ��� CPU, HSB, PBA, PBB ����� ����:
1. RCOSC(115 kHz)
2. OSC0(450 kHz-16MHz crystal)
3. PLL0(80-240 MHz)
   ��� PLL0 ���������� ����� ����:
	1. OSC0
	2. OSC1
 * ���������� generic clock ��� ������� ������ ����� ����:
 *	1.OSC0
	2.OSC1
	3.PLL0
	4.PLL1
	��� PLL0 ���������� ����� ����:
		1.OSC0
		2.OSC1
	��� PLL1 ���������� ����� ����:
		1.OSC0
		2.OSC1
 * Created: 21.10.2015 10:05:12
 * Author : vvn
 */
 #include "OSC0.h"

void Init(void)
{
	///PM init start
	AVR32_PM.MCCTRL.osc0en = 0;// ��������� osc0
	//set prescalers for domains clock
    // src_clock - clock from osc0/1, pll0,1, ROSC
	//PB(A/B)DIV = 0: PB(A/B) clock equals src_clock.
	//PB(A/B)DIV = 1: PB(A/B) clock equals src_clock divided by 2^(***SEL+1)
	// hsbdiv cpudiv - ��� uc3b0512 ���������
	// �� ������ ����� 12/2 = 6 ���
	AVR32_PM.CKSEL.cpudiv = 1;// �����
	AVR32_PM.CKSEL.cpusel = 0;// src_clock / (2^(cpusel+1)) 
	AVR32_PM.CKSEL.hsbdiv = 1; // �����
	AVR32_PM.CKSEL.hsbsel = 0;// src_clock / (2^(hsbsel+1)) 	
	AVR32_PM.CKSEL.pbadiv = 1;// �����
	AVR32_PM.CKSEL.pbasel = 0;// src_clock / (2^(pbasel+1)) 
	AVR32_PM.CKSEL.pbbdiv = 1;// �����
	AVR32_PM.CKSEL.pbbsel = 0;// src_clock / (2^(pbbsel+1)) 
	//switch PLLx to selected OSCx
	//Select startup time  for the oscillator.
	AVR32_PM.OSCCTRL0.startup = (AVR32_PM_OSCCTRL0_STARTUP_2048_RCOSC << AVR32_PM_OSCCTRL0_STARTUP_OFFSET);
	//Choose between crystal freq diapason, or external clock(no crystall)
	//OSC0_MODE_VALUE:0 to 7
	//0: External clock connected on XIN, XOUT can be used as an I/O (no crystal)
	//1 to 3: reserved
	//4: Crystal is connected to XIN/XOUT - Oscillator is used with gain G0 ( XIN from 0.4               //MHz to 0.9 MHz ).
	//5: Crystal is connected to XIN/XOUT - Oscillator is used with gain G1 ( XIN from 0.9 //MHz to 3.0 MHz ).
	//6: Crystal is connected to XIN/XOUT - Oscillator is used with gain G2 ( XIN from 3.0 //MHz to 8.0 MHz ).
	//7: Crystal is connected to XIN/XOUT - Oscillator is used with gain G3 ( XIN from 8.0 //Mhz).
	//AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G0                 0x00000004
	//AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G1                 0x00000005
	//AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G2                 0x00000006
	//AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G3                 0x00000007
	AVR32_PM.OSCCTRL0.mode = AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G3;// 12 MHZ
	//OSCx - enable
	AVR32_PM.MCCTRL.osc0en = 1;
	///wait OSCx ready
	while(AVR32_PM.POSCSR.osc0rdy == 0);
	// Set a flash wait state depending on the new cpu frequency.
	//if(main clock > 30mhz) flashc_set_wait_state(1);
	//else flashc_set_wait_state(0);
	// Set a flash wait state depending on the new cpu frequency.
	//u_avr32_flashc_fcr_t u_avr32_flashc_fcr = {AVR32_FLASHC.fcr};
	AVR32_FLASHC.FCR.fws = 0;// cpu clock 06 mhz
	// sysclk set source
	//Main Clock Select
	//0: The slow clock is the source for the main clock
	//1: Oscillator 0 is source for the main clock
	//2: PLL0 is source for the main clock
	//3: Reserved
	AVR32_PM.MCCTRL.mcsel = AVR32_PM_MCSEL_OSC0;// OSC0
	//////////////////////PM init end
}