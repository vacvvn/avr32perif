 /*
 * OSC1_PLL0.c
 * sync clock from: crystal - OSC0 - PLL0
 * Created: 21.10.2015 14:09:07
 *  Author: vvn
 */ 
 /*
 * PMconfig.c
 * ������������� power manager ��� uc3a0512
 * ���������� sync clock ��� CPU, HSB, PBA, PBB ����� ����:
1. RCOSC(115 kHz)
2. OSC0(450 kHz-16MHz crystal)
3. PLL0(80-240 MHz)
   ��� PLL0 ���������� ����� ����:
	1. OSC0
	2. OSC1
 * ���������� generic clock ��� ������� ������ ����� ����:
 *	1.OSC0
	2.OSC1
	3.PLL0
	4.PLL1
	��� PLL0 ���������� ����� ����:
		1.OSC0
		2.OSC1
	��� PLL1 ���������� ����� ����:
		1.OSC0
		2.OSC1
 * Created: 21.10.2015 10:05:12
 * Author : vvn
 */
 #include "OSC1_PLL0.h"

void Init(void)
{
	///PM init start
	AVR32_PM.MCCTRL.osc1en = 0;// ��������� osc0
	//set prescalers for domains clock
    // src_clock - clock from osc0/1, pll0,1, ROSC
	//PB(A/B)DIV = 0: PB(A/B) clock equals src_clock.
	//PB(A/B)DIV = 1: PB(A/B) clock equals src_clock divided by 2^(***SEL+1)
	// hsbdiv cpudiv - ��� uc3b0512 ���������
	AVR32_PM.CKSEL.cpudiv = 1;// �����
	AVR32_PM.CKSEL.cpusel = 0;// src_clock / (2^(cpusel+1)) 
	AVR32_PM.CKSEL.hsbdiv = 1; // �����
	AVR32_PM.CKSEL.hsbsel = 0;// src_clock / (2^(hsbsel+1)) 	
	AVR32_PM.CKSEL.pbadiv = 1;// �����
	AVR32_PM.CKSEL.pbasel = 0;// src_clock / (2^(pbasel+1)) 
	AVR32_PM.CKSEL.pbbdiv = 1;// �����
	AVR32_PM.CKSEL.pbbsel = 0;// src_clock / (2^(pbbsel+1)) 
	//switch PLLx to selected OSCx
	//Select startup time  for the oscillator.
	AVR32_PM.OSCCTRL1.startup = (AVR32_PM_OSCCTRL1_STARTUP_2048_RCOSC << AVR32_PM_OSCCTRL1_STARTUP_OFFSET);
	//Choose between crystal freq diapason, or external clock(no crystall)
	//OSC0_MODE_VALUE:0 to 7
	//0: External clock connected on XIN, XOUT can be used as an I/O (no crystal)
	//1 to 3: reserved
	//4: Crystal is connected to XIN/XOUT - Oscillator is used with gain G0 ( XIN from 0.4               //MHz to 0.9 MHz ).
	//5: Crystal is connected to XIN/XOUT - Oscillator is used with gain G1 ( XIN from 0.9 //MHz to 3.0 MHz ).
	//6: Crystal is connected to XIN/XOUT - Oscillator is used with gain G2 ( XIN from 3.0 //MHz to 8.0 MHz ).
	//7: Crystal is connected to XIN/XOUT - Oscillator is used with gain G3 ( XIN from 8.0 //Mhz).
	//AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G0                 0x00000004
	//AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G1                 0x00000005
	//AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G2                 0x00000006
	//AVR32_PM_OSCCTRL0_MODE_CRYSTAL_G3                 0x00000007
	AVR32_PM.OSCCTRL1.mode = AVR32_PM_OSCCTRL1_MODE_CRYSTAL_G3;// 12 MHZ
	//OSCx - enable
	AVR32_PM.MCCTRL.osc1en = 1;
	///wait OSCx ready
	while(AVR32_PM.POSCSR.osc1rdy == 0);
	//set PLLx divider, multiplier
	AVR32_PM.PLL[0].pllen = 0;
	//Fvco= (PLLMUL+1)/(PLLDIV) � fOSC if PLLDIV > 0.
	//Fvco= 2*(PLLMUL+1) � fOSC if PLLDIV = 0.
	//If PLLOPT[1] field is set to 0:
	//Fpll = Fvco.
	//If PLLOPT[1] field is set to 1:
	//Fpll = fVCO/ 2
	//!!!!Note that the MUL field cannot be equal to 0 or 1, or the behavior of the PLL will be //undefined
	AVR32_PM.PLL[0].pllmul = 9;//uint32_t pllctrl = 0;
	AVR32_PM.PLL[0].plldiv = 1;// Fvco = (9 + 1)/1 * 12mhz = 120mhz

	//PLL option
	//Select the operating range for the PLL.
	//PLLOPT[0]: Select the VCO frequency range. 0 - 160MHz<fvco<240MHz
	// 1 - 80MHz<fvco<180MHz
	//PLLOPT[1]: Enable the extra output divider. 0 - fPLL = fvco
	//1 - fPLL= fvco/2
	//PLLOPT[2]: Disable the Wide-Bandwidth mode (Wide-Bandwidth mode allows a faster //startup time and out-of-lock time).
	//pllctrl |= (PLLOPT) ;
	AVR32_PM.PLL[0].pllopt = (0x01 | 0x04);// 80-180mhz, fpll = fvco, WBM disa

	//select pll OSC source
	//0: Oscillator 0 is the source for the PLL.
	//1: Oscillator 1 is the source for the PLL.
	AVR32_PM.PLL[0].pllosc = 1;//pllctrl |= (PLLOSC);

    // enable pll0
	AVR32_PM.PLL[0].pllen = 1;
	//wait while PLL0 locked
	while(AVR32_PM.POSCSR.lock0 == 0);
	// Set a flash wait state depending on the new cpu frequency.
	//if(main clock > 30mhz) flashc_set_wait_state(1);
	//else flashc_set_wait_state(0);
	// Set a flash wait state depending on the new cpu frequency.
	//u_avr32_flashc_fcr_t u_avr32_flashc_fcr = {AVR32_FLASHC.fcr};
	AVR32_FLASHC.FCR.fws = 1;// cpu clock 60 mhz
	// sysclk set source
	//Main Clock Select
	//0: The slow clock is the source for the main clock
	//1: Oscillator 0 is source for the main clock
	//2: PLL0 is source for the main clock
	//3: Reserved
	AVR32_PM.MCCTRL.mcsel = AVR32_PM_MCSEL_PLL0;// pll0
	//////////////////////PM init end
} 
